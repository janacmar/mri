const mriAddon = require('../build/Release/mriAddon');
const { workerData, parentPort } = require('worker_threads');

parentPort.postMessage(new Float64Array(mriAddon.levelSet(workerData.outerRadius, workerData.innerRadius,
                                                          workerData.centerY, workerData.centerX, workerData.K1, workerData.K2,
                                                          workerData.height, workerData.width, workerData.buffer)));