const mriAddon = require('../build/Release/mriAddon');
const { workerData, parentPort } = require('worker_threads');

parentPort.postMessage(new Float64Array(mriAddon.optf(workerData.outerRadius, workerData.centerY, workerData.centerX,
                                                      workerData.height, workerData.width, workerData.buffer)));