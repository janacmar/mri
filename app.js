const express = require('express');
const bodyParser = require('body-parser');
const mriAddon = require('./build/Release/mriAddon');
const { Worker } = require('worker_threads');

const app = express();
const port = 8080;

app.use(express.static("public"));
app.use(bodyParser.json({limit: '500mb'}));

const callWorker = (path, data) => {
    return new Promise((resolve, reject) => {
        const worker = new Worker(path, {workerData: data});
        worker.on('message', resolve);
        worker.on('error', reject);
        worker.on('exit', (code) => {
            if (code !== 0)
                reject(new Error(`stopped with  ${code} exit code`));
        });
    });
};

app.post("/segmentation", async (req, res) => {
    let height = req.body.parameters.height;
    let width = req.body.parameters.width;
    let centerX = req.body.parameters.inputParameters.centerX;
    let centerY = req.body.parameters.inputParameters.centerY;
    let outerX = req.body.parameters.inputParameters.outerX;
    let outerY = req.body.parameters.inputParameters.outerY;
    let innerX = req.body.parameters.inputParameters.innerX;
    let innerY = req.body.parameters.inputParameters.innerY;
    let K1 = req.body.parameters.inputParameters.K1 * Math.pow(10.0, -6.0);
    let K2 = req.body.parameters.inputParameters.K2 * Math.pow(10.0, -6.0);
    let outerRadius = Math.sqrt(Math.pow(outerX - centerX, 2.0) + Math.pow(outerY - centerY, 2.0));
    let innerRadius = Math.sqrt(Math.pow(innerX - centerX, 2.0) + Math.pow(innerY - centerY, 2.0));
    let pixelData = new Float64Array(req.body.pixelData);
    let segmented = await callWorker('./workers/levelSet.js', {outerRadius: outerRadius,
                                                                         innerRadius: innerRadius,
                                                                         centerY: centerY,
                                                                         centerX: centerX,
                                                                         K1: K1,
                                                                         K2: K2,
                                                                         height: height,
                                                                         width: width,
                                                                         buffer: pixelData.buffer});
    res.send(Object.values(segmented));
});

app.post("/optf", async (req, res) => {
    let height = req.body.parameters.height;
    let width = req.body.parameters.width;
    let centerX = req.body.parameters.inputParameters.centerX;
    let centerY = req.body.parameters.inputParameters.centerY;
    let outerX = req.body.parameters.inputParameters.outerX;
    let outerY = req.body.parameters.inputParameters.outerY;
    let outerRadius = Math.sqrt(Math.pow(outerX - centerX, 2.0) + Math.pow(outerY - centerY, 2.0));
    let targetSegmented = new Float64Array(req.body.targetSegmented);
    let sourceSegmented = new Float64Array(req.body.sourceSegmented);
    let sourceOriginal = new Float64Array(req.body.sourceOriginal);
    let mergedArray = new Float64Array(width * height * 3);
    mergedArray.set(targetSegmented);
    mergedArray.set(sourceSegmented, width * height);
    mergedArray.set(sourceOriginal, width * height * 2);
    let optf = await callWorker('./workers/optf.js', {outerRadius: outerRadius,
                                                                centerY: centerY,
                                                                centerX: centerX,
                                                                height: height,
                                                                width: width,
                                                                buffer: mergedArray.buffer});
    res.send(Object.values(optf));
});

app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});