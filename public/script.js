const dotColor = [0, 191, 255];
const innerCircleColor = [60, 255, 60];
const outerCircleColor = [249, 255, 47];
const contourLineColor = [255, 0, 0];
const dartboardColors = [
    [255, 0, 0],
    [255, 255, 0],
    [0, 255, 0],
    [0, 255, 255],
    [0, 0, 255],
    [255, 0, 255],
    [255, 255, 255]
];

for (let i = 1; i <= 9; i++)
    cornerstone.enable(document.getElementById("element" + i.toString()));

let hematocrit = 0;
let stops = [];
let files = [[], []];
let dicoms = [];
let initialDICOM = {
    parameters: {
        height: 0,
        width: 0,
        inputParameters: {
            outerX: 0,
            outerY: 0,
            innerX: 0,
            innerY: 0,
            centerX: 0,
            centerY: 0,
            K1: 4.5,
            K2: 4.5
        }
    },
    sliceLocation: null,
    pixelData: [],
    segmented: [],
    layers: []
};

for (let i = 0; i < 9; i++)
    dicoms[i] = JSON.parse(JSON.stringify(initialDICOM));

/**
 * Returns array's maximum.
 *
 * @param {Object} arr Non-empty array of comparable elements.
 * @return {number} Array's maximum.
 */
function arrayMax(arr) {
    let max = arr[0];

    for (let i = 1; i < arr.length; i++)
        if (arr[i] > max)
            max = arr[i];

    return max;
}

/**
 * Returns array's minimum.
 *
 * @param {Object} arr Non-empty array of comparable elements.
 * @return {number} Array's minimum.
 */
function arrayMin(arr) {
    let min = arr[0];

    for (let i = 1; i < arr.length; i++)
        if (arr[i] < min)
            min = arr[i];

    return min;
}

/**
 * Returns dot product of two vectors.
 *
 * @param {number[]} u First vector (at least 2 elements).
 * @param {number[]} v Second vector (at least 2 elements).
 * @returns {number} Vectors' dot product.
 */
function dot(u, v) {
    return u[0] * v[0] + u[1] * v[1];
}

/**
 * Returns norm of a vector.
 *
 * @param {number[]} u Vector (at least 2 elements).
 * @returns {number} Vector's norm.
 */
function norm(u) {
    return Math.sqrt(dot(u, u));
}

/**
 * Creates HTML button with given onclick attribute and inner text.
 *
 * @param {string} onclick Onclick attribute value.
 * @param {string} text Inner text.
 * @returns {HTMLButtonElement} Created button.
 */
function createButton(onclick, text) {
    let button = document.createElement("button");
    button.setAttribute("onclick", onclick);
    button.innerText = text;
    return button;
}

/**
 * Exports registered T1 map to csv.
 */
function save() {
    let height = dicoms[8].parameters.height;
    let width = dicoms[8].parameters.width;
    let csv = '';
    for (let i = 0; i < height; i++) {
        csv += dicoms[8].pixelData[i * width].toString();
        for (let j = 1; j < width; j++)
            csv += ',' + dicoms[8].pixelData[i * width + j].toString();
        csv += '\n';
    }

    let csvData = new Blob([csv], { type: 'text/csv' });
    let csvUrl = URL.createObjectURL(csvData);

    let hiddenElement = document.createElement('a');
    hiddenElement.href = csvUrl;
    hiddenElement.target = '_blank';
    hiddenElement.download = 'export.csv';
    hiddenElement.click();
}

/**
 * Shows all hidden HTML buttons.
 */
function showMore() {
    const buttons = document.getElementById("buttons");
    buttons.classList.toggle("more");
    buttons.textContent = '';
    buttons.appendChild(createButton("registration(0)", "Register (source -> target)"));
    buttons.appendChild(createButton("registration(1)", "Register (source <- target)"));
    buttons.appendChild(createButton("computeT1(0, false)", "Register (T1s -> T1t)"));
    buttons.appendChild(createButton("computeT1(1, false)", "Register (T1s <- T1t)"));
    buttons.appendChild(createButton("computeT1(0, true)", "ECV (source -> target)"));
    buttons.appendChild(createButton("computeT1(1, true)", "ECV (source <- target)"));
    buttons.appendChild(createButton("showLess()", "Show less"));
}

/**
 * Hides all hidden HTML buttons.
 */
function showLess() {
    const buttons = document.getElementById("buttons");
    buttons.classList.toggle("more");
    buttons.textContent = '';
    buttons.appendChild(createButton("registration(0)", "Register (source -> target)"));
    buttons.appendChild(createButton("computeT1(0, true)", "ECV (source -> target)"));
    buttons.appendChild(createButton("showMore()", "Show more"));
}

/**
 * Zooms in given DICOM image.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right, 2 for registered, 5 for left popup T1,
 *                                               6 for right popup T1, 7 for ECV, 8 for registered T1).
 */
function zoomIn(dicom) {
    if (dicoms[dicom].parameters.width === 0)
        return;
    let element = document.getElementById("element" + (dicom + 1).toString());
    const viewport = cornerstone.getViewport(element);
    viewport.scale += 0.25;
    cornerstone.setViewport(element, viewport);
}

/**
 * Zooms out given DICOM image.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right, 2 for registered, 5 for left popup T1,
 *                                               6 for right popup T1, 7 for ECV, 8 for registered T1).
 */
function zoomOut(dicom) {
    if (dicoms[dicom].parameters.width === 0)
        return;
    let element = document.getElementById("element" + (dicom + 1).toString());
    const viewport = cornerstone.getViewport(element);
    viewport.scale -= 0.25;
    cornerstone.setViewport(element, viewport);
}

/**
 * Updates window center of given DICOM image's canvas.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right, 2 for registered, 3 for left thumbnail T1, 4 for right thumbnail T1,
 *                                               5 for left popup T1, 6 for right popup T1, 7 for ECV, 8 for registered T1).
 * @param {string} value New window center value.
 */
function updateWindowCenter(dicom, value) {
    if (dicoms[dicom].parameters.width === 0)
        return;
    let element = document.getElementById("element" + (dicom + 1).toString());
    let viewport = cornerstone.getViewport(element);
    viewport.voi.windowCenter = parseFloat(value);
    cornerstone.setViewport(element, viewport);
}

/**
 * Updates window width of given DICOM image's canvas.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right, 2 for registered, 3 for left thumbnail T1, 4 for right thumbnail T1,
 *                                               5 for left popup T1, 6 for right popup T1, 7 for ECV, 8 for registered T1).
 * @param {string} value New window width value.
 */
function updateWindowWidth(dicom, value) {
    if (dicoms[dicom].parameters.width === 0)
        return;
    let element = document.getElementById("element" + (dicom + 1).toString());
    let viewport = cornerstone.getViewport(element);
    viewport.voi.windowWidth = parseFloat(value);
    cornerstone.setViewport(element, viewport);
}

/**
 * Returns radius of circle defined by its center and a point through which the circle goes.
 *
 * @param {number} centerX X coordinate of circle's center.
 * @param {number} centerY Y coordinate of circle's center.
 * @param {number} X X coordinate of point through which the circle goes.
 * @param {number} Y Y coordinate of point through which the circle goes.
 * @returns {number} Circle's radius.
 */
function countRadius(centerX, centerY, X, Y) {
    return Math.sqrt(Math.pow(X - centerX, 2.0) + Math.pow(Y - centerY, 2.0));
}

/**
 * Returns Cornerstone Image object for given parameters and pixel data, if pixel data is RGBA, object must be colorized afterwards.
 *
 * @param {number} height Image's height.
 * @param {number} width Image's width.
 * @param {Object} pixelData Either DICOM MR image greyscale pixel data (Uint16Array) or some RGBA pixel data (number[]).
 * @returns {Object} Resulting Cornerstone Image object.
 */
function getImage(height, width, pixelData) {
    function getPixelData () {
        return pixelData;
    }

    let image = {
        minPixelValue : arrayMin(pixelData),
        maxPixelValue : arrayMax(pixelData),
        slope: 1,
        intercept: 0,
        windowCenter : 376,
        windowWidth : 917,
        getPixelData: getPixelData,
        rows: height,
        columns: width,
        height: height,
        width: width,
        color: false,
        sizeInBytes: width * height * 2
    };

    return image;
}

/**
 * Returns colorized Cornerstone Image object for given Cornerstone Image object.
 *
 * @param image Cornerstone Image object with RGBA pixel data.
 * @returns {Object} Colorized Cornerstone Image object.
 */
function getColorImage(image) {
    const canvas = document.createElement('canvas');
    canvas.width = image.width;
    canvas.height = image.height;
    const canvasContext = canvas.getContext('2d', {
        desynchronized: true
    });
    const imageData = canvasContext.createImageData(image.width, image.height);
    imageData.data = image.getPixelData;
    canvasContext.putImageData(imageData, 0, 0);

    function getImageData () {
        return imageData;
    }

    function getCanvas () {
        return canvas;
    }

    image.color = true;
    image.sizeInBytes = image.width * image.height * 4;
    image.render = cornerstone.renderColorImage;
    image.getImageData = getImageData;
    image.getCanvas = getCanvas;

    return image;
}

/**
 * Updates given layer of given DICOM image with given pixel data and opacity.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right, 2 for registered, 3 for left thumbnail T1, 4 for right thumbnail T1,
 *                                               5 for left popup T1, 6 for right popup T1, 7 for ECV, 8 for registered T1).
 * @param {number} layer Image layer identifier (0 for base, 1 for center, 2 for outer circle, 3 for inner circle, 4 for dartboard, 5 for contour line).
 * @param {Object} pixelData Either DICOM MR image greyscale pixel data (Uint16Array) or some RGBA pixel data (number[]).
 * @param {number} opacity Opacity (Number between 0 and 1).
 * @param {boolean} color Whether pixel data is RGBA.
 */
function updateLayer(dicom, layer, pixelData, opacity, color) {
    let image = getImage(dicoms[dicom].parameters.height, dicoms[dicom].parameters.width, pixelData);
    if (color) {
        image = getColorImage(image);
    }

    let options = {
        opacity: opacity
    };

    let element = document.getElementById("element" + (dicom + 1).toString());
    cornerstone.removeLayer(element, dicoms[dicom].layers[layer]);
    dicoms[dicom].layers[layer] = cornerstone.addLayer(element, image, options);
    cornerstone.updateImage(element);
}

/**
 * Returns constructed RGBA pixel data with one pixel colored based on parameters.
 *
 * @param {number} width Image's width.
 * @param {number} height Image's height.
 * @param {number} X X coordinate of pixel.
 * @param {number} Y Y coordinate of pixel.
 * @param {number[]} color RGB color (array of 3 numbers).
 * @returns {number[]} Constructed RGBA pixel data.
 */
function makeDot(width, height, X, Y, color) {
    let pixelData = [];

    for (let i = 0; i < 4 * width * height; i++) {
        pixelData[i] = 0;
    }
    if (X > 0 && X < width - 1 && Y > 0 && Y < height - 1) {
        for (let i = -1; i < 2; i++)
            for (let j = -1; j < 2; j++) {
                pixelData[4 * ((Y + i) * width + X + j)] = color[0];
                pixelData[4 * ((Y + i) * width + X + j) + 1] = color[1];
                pixelData[4 * ((Y + i) * width + X + j) + 2] = color[2];
                pixelData[4 * ((Y + i) * width + X + j) + 3] = 255;
            }
    }
    else {
        pixelData[4 * (Y * width + X)] = color[0];
        pixelData[4 * (Y * width + X) + 1] = color[1];
        pixelData[4 * (Y * width + X) + 2] = color[2];
        pixelData[4 * (Y * width + X) + 3] = 255;
    }

    return pixelData;
}

/**
 * Returns constructed RGBA pixel data with a circle colored based on parameters.
 *
 * @param {number} width Image's width.
 * @param {number} height Image's height.
 * @param {number} centerX X coordinate of center.
 * @param {number} centerY Y coordinate of center.
 * @param {number} X X coordinate of point through which the circle goes.
 * @param {number} Y Y coordinate of point through which the circle goes.
 * @param {number[]} color RGB color (array of 3 numbers).
 * @returns {number[]} Constructed RGBA pixel data.
 */
function makeCircle(width, height, centerX, centerY, X, Y, color) {
    let pixelData = [];
    let radius = countRadius(centerX, centerY, X, Y);

    for (let y = 0; y < height; y++)
        for (let x = 0; x < width; x++)
            if (Math.round(countRadius(centerX, centerY, x, y)) === Math.round(radius)) {
                pixelData[4 * (y * width + x)] = color[0];
                pixelData[4 * (y * width + x) + 1] = color[1];
                pixelData[4 * (y * width + x) + 2] = color[2];
                pixelData[4 * (y * width + x) + 3] = 255;
            }
            else {
                pixelData[4 * (y * width + x)] = 0;
                pixelData[4 * (y * width + x) + 1] = 0;
                pixelData[4 * (y * width + x) + 2] = 0;
                pixelData[4 * (y * width + x) + 3] = 0;
            }

    return pixelData;
}

/**
 * Returns constructed segmentation dartboard based on given parameters and data.
 *
 * @param {number} centerX X coordinate of center.
 * @param {number} centerY Y coordinate of center.
 * @param {number} outerX X coordinate of point through which the outer circle goes.
 * @param {number} outerY Y coordinate of point through which the outer circle goes.
 * @param {number[]} segmented Segmented pixel data.
 * @param {number} width Image's width.
 * @param {number} height Image's height.
 * @returns {boolean[][]} Constructed dartboard (for each of 6 segments and center a mask indicating which pixels should be colored).
 */
function makeDartboard(centerX, centerY, outerX, outerY, segmented, height, width) {
    let masks = [];
    let v1 = [outerX - centerX + 0.5, outerY - centerY + 0.5];
    let nv1 = norm(v1);
    v1 = [v1[0] / nv1, v1[1] / nv1];

    for (let i = 0; i < 7; i++) {
        let mask = [];
        if (i < 6)
            for (let y = 0; y < height; y++)
                for (let x = 0; x < width; x++) {
                    let v2 = [x - centerX + 0.5, y - centerY + 0.5];
                    let nv2 = norm(v2);
                    let sector = null;
                    if (nv2 === 0)
                        sector = 0;
                    else {
                        v2 = [v2[0] / nv2, v2[1] / nv2];
                        let arg = Math.min(1, Math.max(-1, dot(v1, v2)));
                        let phi = (Math.acos(arg) * 180) / Math.PI;
                        if (v1[0] * v2[1] - v1[1] * v2[0] > 0)
                            phi = 360 - phi;
                        sector = (Math.floor(phi / 60)) % 6;
                    }
                    mask[y * width + x] = (segmented[y * width + x] < 0 && sector === i);
                }
        else {
            let radius = countRadius(centerX, centerY, outerX, outerY) / 2;
            for (let y = 0; y < height; y++)
                for (let x = 0; x < width; x++)
                    mask[y * width + x] = segmented[y * width + x] > 0 && countRadius(centerX, centerY, x, y) <= radius;
        }
        masks[i] = mask;
    }

    return masks;
}

/**
 * For given DICOM image returns constructed RGBA pixel data with colored segmentation dartboard.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right), DICOM image must be segmented.
 * @returns {number[]} Constructed RGBA pixel data.
 */
function makeColoredDartboard(dicom) {
    let pixelData = [];
    let centerX = dicoms[dicom].parameters.inputParameters.centerX;
    let centerY = dicoms[dicom].parameters.inputParameters.centerY;
    let outerX = dicoms[dicom].parameters.inputParameters.outerX;
    let outerY = dicoms[dicom].parameters.inputParameters.outerY;
    let height = dicoms[dicom].parameters.height;
    let width = dicoms[dicom].parameters.width;

    let masks = makeDartboard(centerX, centerY, outerX, outerY, dicoms[dicom].segmented, height, width);

    for (let i = 0; i < 4 * width * height; i++)
        pixelData[i] = 0;
    for (let j = 0; j < 6; j++)
        for (let i = 0; i < width * height; i++)
            if (masks[j][i]) {
                pixelData[4 * i] = dartboardColors[j][0];
                pixelData[4 * i + 1] = dartboardColors[j][1];
                pixelData[4 * i + 2] = dartboardColors[j][2];
                pixelData[4 * i + 3] = 255;
            }

    return pixelData;
}

/**
 * For given DICOM image returns constructed RGBA pixel data with colored contour line.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right), DICOM image must be segmented.
 * @param {number[]} color RGB color (array of 3 numbers).
 * @returns {number[]} Constructed RGBA pixel data.
 */
function makeContourLine(dicom, color) {
    let height = dicoms[dicom].parameters.height;
    let width = dicoms[dicom].parameters.width;
    let v = dicoms[dicom].segmented;
    let u = [];
    let pixelData = [];

    for (let i = 0; i < height; i++)
        for (let j = 0; j < width; j++)
            if (i > 0 && j > 0 && (v[i * width + j] * v[(i - 1) * width + j] < 0 || v[i * width + j] * v[i * width + j - 1] < 0))
                u[i * width + j] = true;
            else
                u[i * width + j] = false;

    for (let i = 0; i < width * height; i++)
        if (u[i]) {
            pixelData[4 * i] = color[0];
            pixelData[4 * i + 1] = color[1];
            pixelData[4 * i + 2] = color[2];
            pixelData[4 * i + 3] = 255;
        }
        else {
            pixelData[4 * i] = 0;
            pixelData[4 * i + 1] = 0;
            pixelData[4 * i + 2] = 0;
            pixelData[4 * i + 3] = 0;
        }

    return pixelData;
}

/**
 * For given DICOM image shows center, inner circle and outer circle based on its parameters.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right).
 */
function makeCircles(dicom) {
    let height = dicoms[dicom].parameters.height;
    let width = dicoms[dicom].parameters.width;
    let centerX = dicoms[dicom].parameters.inputParameters.centerX;
    let centerY = dicoms[dicom].parameters.inputParameters.centerY;
    let outerX = dicoms[dicom].parameters.inputParameters.outerX;
    let outerY = dicoms[dicom].parameters.inputParameters.outerY;
    let innerX = dicoms[dicom].parameters.inputParameters.innerX;
    let innerY = dicoms[dicom].parameters.inputParameters.innerY;

    cornerstone.removeLayer(document.getElementById("element" + (dicom + 1).toString()), dicoms[dicom].layers[4]);
    cornerstone.removeLayer(document.getElementById("element" + (dicom + 1).toString()), dicoms[dicom].layers[5]);
    let pixelData1 = makeDot(width, height, centerX, centerY, dotColor);
    updateLayer(dicom, 1, pixelData1, 1.0, true);
    let pixelData2 = makeCircle(width, height, centerX, centerY, outerX, outerY, outerCircleColor);
    updateLayer(dicom, 2, pixelData2, 1.0, true);
    let pixelData3 = makeCircle(width, height, centerX, centerY, innerX, innerY, innerCircleColor);
    updateLayer(dicom, 3, pixelData3, 1.0, true);
}

/**
 * For given DICOM image checks validity of its input parameters' HTML input elements and updates both HTML and JS data accordingly.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right).
 */
function inputParametersUpdate(dicom) {
    for (const [key, value] of Object.entries(dicoms[dicom].parameters.inputParameters)) {
        let parameter = document.getElementById(key + (dicom + 1).toString());
        if (parameter.checkValidity()) {
            dicoms[dicom].parameters.inputParameters[key] = Number(parameter.value);
        }
        else {
            parameter.value = value;
        }
    }

    if (dicoms[dicom].parameters.width === 0)
        return;

    dicoms[dicom].segmented = [];
    makeCircles(dicom);
}

/**
 * Updates hematocrit.
 */
function hematocritUpdate() {
    let HCTInput = document.getElementById("hematocrit");
    if (HCTInput.checkValidity()) {
        hematocrit = Number(HCTInput.value);
    }
    else {
        HCTInput.value = hematocrit;
    }
}

/**
 * Displays T1 time for given pixel.
 *
 * @param {Object} event Mousemove event.
 */
function displayT1(event) {
    let element = document.getElementById("element9");
    const pixelCoords = cornerstone.pageToPixel(element, event.pageX, event.pageY);
    let x = Math.round(pixelCoords.x);
    let y = Math.round(pixelCoords.y);
    let width = dicoms[8].parameters.width;
    let height = dicoms[8].parameters.height;

    if (x < 0) {
        x = 0;
    }
    if (x > width - 1) {
        x = width - 1;
    }
    if (y < 0) {
        y = 0;
    }
    if (y > height - 1) {
        y = height - 1;
    }

    document.getElementById("overlay").textContent = "T1: " + dicoms[8].pixelData[y * width + x].toString() + " ms";
}

/**
 * Pans given DICOM image.
 *
 * @param {Object} event Mousedown event.
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right, 2 for registered, 5 for left popup T1,
 *                                               6 for right popup T1, 7 for ECV, 8 for registered T1).
 */
function pan(event, dicom) {
    if (dicoms[dicom].parameters.width === 0)
        return;
    let element = document.getElementById("element" + (dicom + 1).toString());
    let lastX = event.pageX;
    let lastY = event.pageY;

    function mouseMoveHandler(event) {
        const deltaX = event.pageX - lastX;
        const deltaY = event.pageY - lastY;
        lastX = event.pageX;
        lastY = event.pageY;

        const viewport = cornerstone.getViewport(element);
        viewport.translation.x += (deltaX / viewport.scale);
        viewport.translation.y += (deltaY / viewport.scale);
        cornerstone.setViewport(element, viewport);
    }

    function mouseUpHandler() {
        document.removeEventListener('mousemove', mouseMoveHandler);
        document.getElementById("element9").setAttribute("onmousemove", "displayT1(event)");
        document.removeEventListener('mouseup', mouseUpHandler);
    }

    document.getElementById("element9").removeAttribute("onmousemove");
    document.addEventListener('mousemove', mouseMoveHandler);
    document.addEventListener('mouseup', mouseUpHandler);
}

/**
 * For given DICOM image sets maximal possible x and y coordinates of its input parameters.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right).
 */
function setMaxes(dicom) {
    for (const element of ["centerX", "outerX", "innerX"]) {
        let parameter = document.getElementById(element + (dicom + 1).toString());
        parameter.setAttribute("max", (dicoms[dicom].parameters.width - 1).toString());
    }
    for (const element of ["centerY", "outerY", "innerY"]) {
        let parameter = document.getElementById(element + (dicom + 1).toString());
        parameter.setAttribute("max", (dicoms[dicom].parameters.height - 1).toString());
    }
}

/**
 * Resets HTML range input values of given DICOM image's window width and window center to default values.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right, 2 for registered, 5 for left popup T1,
 *                                               6 for right popup T1, 7 for ECV, 8 for registered T1).
 */
function resetRanges(dicom) {
    if (dicom === 5 || dicom === 6 || dicom === 8) {
        document.getElementById("wc" + (dicom + 1).toString()).value = 850;
        document.getElementById("ww" + (dicom + 1).toString()).value = 1500;
    }
    else {
        document.getElementById("wc" + (dicom + 1).toString()).value = 376;
        document.getElementById("ww" + (dicom + 1).toString()).value = 917;
    }
}

/**
 * Resets given DICOM image's window width, window center, scale, and position to default values.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right, 2 for registered, 5 for left popup T1,
 *                                               6 for right popup T1, 7 for ECV, 8 for registered T1).
 */
function resetAdjusting(dicom) {
    let element = document.getElementById("element" + (dicom + 1).toString());
    cornerstone.enable(element);
    if (dicom === 5 || dicom === 6 || dicom === 8) {
        updateWindowCenter(dicom, "850");
        updateWindowWidth(dicom, "1500");
    }
    else {
        updateWindowCenter(dicom, "376");
        updateWindowWidth(dicom, "917");
    }
    resetRanges(dicom);
}

/**
 * Resets given DICOM image as if it was empty, only canvas remains.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right).
 */
function resetDICOM(dicom) {
    let element = document.getElementById("element" + (dicom + 1).toString());
    for (let i = 0; i < 6; i++)
        cornerstone.removeLayer(element, dicoms[dicom].layers[i]);
    dicoms[dicom] = JSON.parse(JSON.stringify(initialDICOM));

    for (const [key, value] of Object.entries(dicoms[dicom].parameters.inputParameters)) {
        let parameter = document.getElementById(key + (dicom + 1).toString());
        parameter.value = value;
    }

    setMaxes(dicom);
    resetRanges(dicom);
    element.setAttribute("class", "contextMenu");
    element.setAttribute('onmousedown', "pan(event, " + dicom.toString() + ")");
}

/**
 * Toggles given popup.
 *
 * @param {number} popup Popup identifier (0 for registered popup, 1 for ECV popup, 2 for registered T1 popup).
 */
function togglePopup(popup) {
    document.getElementById("popup" + (popup + 1).toString()).classList.toggle("show");
    resetRanges(2);
    for (let i = 5; i <= 8; i++)
        resetRanges(i);
}

/**
 * Sets given input parameter of given DICOM image.
 *
 * @param {Object} event Mousedown event.
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right).
 * @param {string} parameter Parameter that should be set (center, outer or inner).
 */
function setParameter(event, dicom, parameter) {
    let element = document.getElementById("element" + (dicom + 1).toString());
    const pixelCoords = cornerstone.pageToPixel(element, event.pageX, event.pageY);
    let x = Math.round(pixelCoords.x);
    let y = Math.round(pixelCoords.y);

    if (x < 0) {
        x = 0;
    }
    if (x > dicoms[dicom].parameters.width - 1) {
        x = dicoms[dicom].parameters.width - 1;
    }
    if (y < 0) {
        y = 0;
    }
    if (y > dicoms[dicom].parameters.height - 1) {
        y = dicoms[dicom].parameters.height - 1;
    }

    document.getElementById(parameter + "X" + (dicom + 1).toString()).value = x;
    document.getElementById(parameter + "Y" + (dicom + 1).toString()).value = y;
    element.setAttribute("class", "move");
    element.setAttribute('onmousedown', "pan(event, " + dicom.toString() + ")");
    inputParametersUpdate(dicom);
}

/**
 * Sets listener for setting given parameter of given DICOM image.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right).
 * @param {string} parameter Parameter that should be set (center, outer or inner).
 */
function setParameterListener(dicom, parameter) {
    if (dicoms[dicom].parameters.width === 0)
        return;
    let element = document.getElementById("element" + (dicom + 1).toString());
    element.setAttribute("class", "crosshair");
    element.setAttribute('onmousedown', "setParameter(event, " + dicom.toString() + ", \"" + parameter + "\")");
}

/**
 * Sets dimensions of given Cornerstone enabled element and its canvas based on given parameters (also resets scale and position).
 *
 * @param {HTMLElement} element A DICOM image's Cornerstone enabled element.
 * @param {number} height
 * @param {number} width
 */
function countDimensions(element, height, width) {
    if (height > width) {
        let w = Math.round(width / height * 10000) / 100;
        element.setAttribute("style", "width:" + w.toString() + "%;height:100%");
    }
    else {
        let h = Math.round(height / width * 10000) / 100;
        element.setAttribute("style", "width:100%;height:" + h.toString() + "%");
    }
    cornerstone.enable(element);
}

/**
 * Reads file loaded by given reader and for given DICOM image updates JS object's dimensions and pixel data.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right, 3 for left thumbnail T1, 4 for right thumbnail T1).
 * @param {Object} reader Loaded file reader reading file as array buffer.
 * @throws Throws exception if file has wrong DICOM format.
 */
function readDICOM(dicom, reader) {
    // form dataset from DICOM file
    let arrayBuffer = reader.result;
    let byteArray = new Uint8Array(arrayBuffer);
    let dataSet = dicomParser.parseDicom(byteArray);

    let photometricInterpretation = dataSet.string('x00280004');
    let modality = dataSet.string('x00080060');
    if (modality !== "MR" || photometricInterpretation !== "MONOCHROME2")
        throw "Wrong DICOM format, modality must be MR and photometric interpretation must be MONOCHROME2.";

    // get necessary data from dataset
    dicoms[dicom].parameters.height = dataSet.uint16('x00280010');
    dicoms[dicom].parameters.width = dataSet.uint16('x00280011');
    dicoms[dicom].sliceLocation = dataSet.string('x00201041');
    let pixelDataElement = dataSet.elements.x7fe00010;
    let data = dataSet.byteArray.buffer.slice(pixelDataElement.dataOffset, pixelDataElement.dataOffset + pixelDataElement.length);
    dicoms[dicom].pixelData = new Uint16Array(data);
}

/**
 * Views file defined by parameters.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right).
 * @param {number} index File index in given DICOM image's file list.
 */
function viewDICOM(dicom, index) {
    for (let i = 0; i < files[dicom].length; i++)
        document.getElementById("file" + (i + 1).toString() + (dicom+ 1).toString()).removeAttribute("class");
    document.getElementById("file" + (index + 1).toString() + (dicom+ 1).toString()).setAttribute("class",
                                                                                                           "active");
    let file = files[dicom][index];
    let reader = new FileReader();
    reader.readAsArrayBuffer(file);
    reader.onload = function() {
        resetDICOM(dicom);
        let element = document.getElementById('element' + (dicom + 1).toString());

        try {
            readDICOM(dicom, reader);
        }
        catch (e) {
            element.setAttribute("style", "visibility: hidden;");
            return;
        }

        setMaxes(dicom);
        countDimensions(element, dicoms[dicom].parameters.height, dicoms[dicom].parameters.width);
        updateLayer(dicom, 0, dicoms[dicom].pixelData, 1.0, false);
        inputParametersUpdate(dicom);
        element.setAttribute("class", "move");
    };
}

/**
 * Updates given DICOM image's file list.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right).
 * @param {Object} input File input.
 */
function updateFiles(dicom, input) {
    if (input.files.length === 0)
        return;
    files[dicom] = [];
    const list = document.getElementById("list" + (dicom + 1).toString());
    list.textContent = '';
    for (let i = 0; i < input.files.length; i++) {
        files[dicom][i] = input.files[i];
        const li = document.createElement("li");
        li.innerText = input.files[i].name;
        li.setAttribute("id", "file" + (i + 1).toString() + (dicom + 1).toString());
        li.setAttribute("onclick", "viewDICOM(" + dicom.toString() + ", "+i.toString()+")");
        list.appendChild(li);
    }
    viewDICOM(dicom, 0);
}

/**
 * Updates given DICOM image's T1 map.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right).
 * @param {Object} input File input.
 */
function loadT1Map(dicom, input) {
    if (input.files.length === 0)
        return;
    let file = input.files[0];
    let reader = new FileReader();
    reader.readAsArrayBuffer(file);
    reader.onload = function() {
        let element = document.getElementById("element" + (dicom + 4).toString());
        cornerstone.removeLayer(element, dicoms[dicom + 3].layers[0]);
        dicoms[dicom + 3] = JSON.parse(JSON.stringify(initialDICOM));
        try {
            readDICOM(dicom + 3, reader);
        }
        catch (e) {
            input.value = input.defaultValue;
            element.setAttribute("style", "visibility: hidden;");
            return;
        }
        countDimensions(element, dicoms[dicom + 3].parameters.height, dicoms[dicom + 3].parameters.width);
        updateLayer(dicom + 3, 0, dicoms[dicom + 3].pixelData, 1.0, false);
        updateWindowCenter(dicom + 3, "850");
        updateWindowWidth(dicom + 3, "1500");
        const viewport = cornerstone.getViewport(element);
        viewport.colormap = "pet";
        cornerstone.setViewport(element, viewport);
    };
}

/**
 * Checks if given DICOM image's input parameters are ready for segmentation.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right).
 * @returns {boolean}
 */
function checkInputParameters(dicom) {
    let height = dicoms[dicom].parameters.height;
    let width = dicoms[dicom].parameters.width;
    let centerX = dicoms[dicom].parameters.inputParameters.centerX;
    let centerY = dicoms[dicom].parameters.inputParameters.centerY;
    let outerX = dicoms[dicom].parameters.inputParameters.outerX;
    let outerY = dicoms[dicom].parameters.inputParameters.outerY;
    let innerX = dicoms[dicom].parameters.inputParameters.innerX;
    let innerY = dicoms[dicom].parameters.inputParameters.innerY;
    let innerRadius = countRadius(centerX, centerY, innerX, innerY);
    let outerRadius = countRadius(centerX, centerY, outerX, outerY);

    if (innerRadius >= outerRadius) {
        window.alert("Inner radius must be smaller than outer radius.");
        return false;
    }
    if (outerRadius > centerX - 16 || outerRadius > centerY - 16 ||
        outerRadius > width - centerX - 17 || outerRadius > height - centerY - 17) {
        window.alert("Circles are too close to edges.");
        return false;
    }
    return true;
}

/**
 * Stops current process.
 */
function stopProcess() {
    stops[stops.length - 1] = true;
    document.getElementById("cover-spin").classList.toggle("show");
}

/**
 * Performs segmentation of given DICOM image.
 *
 * @param {number} dicom DICOM image identifier (0 for left, 1 for right).
 */
function segmentation(dicom) {
    if (dicoms[dicom].parameters.width === 0 || dicoms[dicom].segmented.length !== 0)
        return;
    if (! checkInputParameters(dicom))
        return;
    document.getElementById("cover-spin").classList.toggle("show");
    let last = stops.length;
    stops.push(false);

    const xhttp = new XMLHttpRequest();
    xhttp.onload = function() {
        if (stops[last])
            return;
        dicoms[dicom].segmented = new Float64Array(JSON.parse(this.responseText));
        let pixelData1 = makeColoredDartboard(dicom);
        updateLayer(dicom, 4, pixelData1, 0.2, true);
        let pixelData2 = makeContourLine(dicom, contourLineColor);
        updateLayer(dicom, 5, pixelData2, 1.0, true);
        let element = document.getElementById("element" + (dicom + 1).toString());
        cornerstone.removeLayer(element, dicoms[dicom].layers[1]);
        cornerstone.removeLayer(element, dicoms[dicom].layers[2]);
        cornerstone.removeLayer(element, dicoms[dicom].layers[3]);
        document.getElementById("cover-spin").classList.toggle("show");
    };
    xhttp.open("POST", "segmentation", true);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.send(JSON.stringify({
        parameters: dicoms[dicom].parameters,
        pixelData: Object.values(dicoms[dicom].pixelData)
    }));
}

/**
 * Performs registration.
 *
 * @param {number} inverse Indicates direction of registration (0 for source -> target, 1 for source <- target).
 */
function registration(inverse) {
    if (dicoms[0].segmented.length === 0 || dicoms[1].segmented.length === 0 ||
        dicoms[0].parameters.width !== dicoms[1].parameters.width || dicoms[0].parameters.height !== dicoms[1].parameters.height) {
        window.alert("Both files must be segmented and both must have same dimensions.");
        return;
    }
    document.getElementById("cover-spin").classList.toggle("show");
    let last = stops.length;
    stops.push(false);

    const xhttp = new XMLHttpRequest();
    xhttp.onload = function() {
        if (stops[last])
            return;
        let pixelData = new Uint16Array(JSON.parse(this.responseText).slice(0, dicoms[0].parameters.width * dicoms[0].parameters.height));
        dicoms[2].parameters.height = dicoms[0].parameters.height;
        dicoms[2].parameters.width = dicoms[0].parameters.width;
        let element = document.getElementById('element3');
        countDimensions(element, dicoms[2].parameters.height, dicoms[2].parameters.width);
        updateLayer(2, 0, pixelData, 1.0, false);
        togglePopup(0);
        document.getElementById("cover-spin").classList.toggle("show");
    };
    xhttp.open("POST", "optf", true);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.send(JSON.stringify({
        parameters: dicoms[(inverse) % 2].parameters,
        targetSegmented: Object.values(dicoms[(1 + inverse) % 2].segmented),
        sourceSegmented: Object.values(dicoms[(inverse) % 2].segmented),
        sourceOriginal: Object.values(dicoms[(inverse) % 2].pixelData)
    }));
}

/**
 * Shows ECV popup with original T1 maps, ECV map and ECV values for each segment.
 *
 * @param {number[]} ecv Array of ECV values (one for each of 6 segments).
 * @param {number[]} ECVMap ECV map pixel data.
 */
function showECV(ecv, ECVMap) {
    const list = document.getElementById("ecv");
    list.textContent = '';
    for (let i = 0; i < 6; i++) {
        const li = document.createElement("li");
        li.innerText = ecv[i].toFixed(2).toString();
        list.appendChild(li);
    }
    for (let i = 5; i <= 6; i++) {
        dicoms[i].parameters.height = dicoms[0].parameters.height;
        dicoms[i].parameters.width = dicoms[0].parameters.width;
        let element = document.getElementById('element' + (i + 1).toString());
        countDimensions(element, dicoms[i].parameters.height, dicoms[i].parameters.width);
        updateLayer(i, 0, dicoms[i - 2].pixelData, 1.0, false);
        resetAdjusting(i);
        const viewport = cornerstone.getViewport(element);
        viewport.colormap = "pet";
        cornerstone.setViewport(element, viewport);
    }
    dicoms[7].parameters.height = dicoms[0].parameters.height;
    dicoms[7].parameters.width = dicoms[0].parameters.width;
    let element = document.getElementById('element8');
    countDimensions(element, dicoms[7].parameters.height, dicoms[7].parameters.width);
    updateLayer(7, 0, Uint16Array.from(ECVMap), 1.0, false);
    resetAdjusting(7);
    const viewport = cornerstone.getViewport(element);
    viewport.colormap = "pet";
    cornerstone.setViewport(element, viewport);
    togglePopup(1);
}

/**
 * Computes T1.
 *
 * @param {number} inverse Indicates direction (0 for source -> target, 1 for source <- target).
 * @param {boolean} ecv Indicates if ecv values should be computed.
 */
function computeT1(inverse, ecv) {
    if (dicoms[0].segmented.length === 0 || dicoms[1].segmented.length === 0 ||
        dicoms[0].parameters.width !== dicoms[1].parameters.width || dicoms[0].parameters.height !== dicoms[1].parameters.height) {
        window.alert("Both files must be segmented and both must have same dimensions.");
        return;
    }
    if (dicoms[3].pixelData.length === 0 || dicoms[4].pixelData.length === 0 ||
        dicoms[0].parameters.width !== dicoms[3].parameters.width || dicoms[0].parameters.height !== dicoms[3].parameters.height ||
        dicoms[0].parameters.width !== dicoms[4].parameters.width || dicoms[0].parameters.height !== dicoms[4].parameters.height) {
        window.alert("Both T1 maps must be loaded and both must have the same dimensions as the segmented files.");
        return;
    }
    if (dicoms[0].sliceLocation !== dicoms[1].sliceLocation || dicoms[0].sliceLocation !== dicoms[3].sliceLocation ||
        dicoms[0].sliceLocation !== dicoms[4].sliceLocation) {
        window.alert('Both files and both T1 maps must have a same Slice Location.');
        return;
    }
    document.getElementById("cover-spin").classList.toggle("show");
    let last = stops.length;
    stops.push(false);

    const xhttp = new XMLHttpRequest();
    xhttp.onload = function() {
        if (stops[last])
            return;
        let width = dicoms[0].parameters.width;
        let height = dicoms[0].parameters.height;
        let transX = new Float64Array(JSON.parse(this.responseText).slice(width * height, width * height * 2));
        let transY = new Float64Array(JSON.parse(this.responseText).slice(width * height * 2));

        function applyDeformation(array) {
            let deformed = [];
            let h1 = 1.0 / Math.max(height, width);
            let h2 = h1;
            for (let y = 0; y < height; y++)
                for (let x = 0; x < width; x++) {
                    let dx = Math.trunc(x - Math.round(transX[y * width + x] / h1));
                    let dy = Math.trunc(y - Math.round(transY[y * width + x] / h2));
                    if (0 <= dx && dx < width && 0 <= dy && dy < height)
                        deformed[y * width + x] = array[Math.abs(dy) * width + Math.abs(dx)];
                    else
                        deformed[y * width + x] = 0;
                }
            return deformed;
        }

        let sourceT1 = dicoms[3].pixelData;
        let targetT1 = dicoms[4].pixelData;
        if (inverse === 0)
            sourceT1 = applyDeformation(sourceT1);
        else
            targetT1 = applyDeformation(targetT1);

        if (ecv === false) {
            let pixelData1;
            if (inverse === 0)
                pixelData1 = sourceT1;
            else
                pixelData1 = targetT1;
            let pixelData2 = makeColoredDartboard((inverse + 1) % 2);
            dicoms[8].parameters.height = height;
            dicoms[8].parameters.width = width;
            dicoms[8].pixelData = pixelData1;
            let element = document.getElementById('element9');
            countDimensions(element, dicoms[8].parameters.height, dicoms[8].parameters.width);
            updateLayer(8, 0, pixelData1, 1.0, false);
            updateLayer(8, 4, pixelData2, 0.2, true);
            resetAdjusting(8);
            document.getElementById("overlay").textContent = "T1:";
            togglePopup(2);
        }
        else {
            let centerX = dicoms[(inverse + 1) % 2].parameters.inputParameters.centerX;
            let centerY = dicoms[(inverse + 1) % 2].parameters.inputParameters.centerY;
            let outerX = dicoms[(inverse + 1) % 2].parameters.inputParameters.outerX;
            let outerY = dicoms[(inverse + 1) % 2].parameters.inputParameters.outerY;
            let masks = makeDartboard(centerX, centerY, outerX, outerY, dicoms[(inverse + 1) % 2].segmented, height, width);
            let centralMask = masks[6];

            function average(T1, mask) {
                let pixSum = 0;
                let pixCount = 0;
                for (let y = 0; y < height; y++)
                    for (let x = 0; x < width; x++) {
                        pixSum += mask[y * width + x] * T1[y * width + x];
                        pixCount += mask[y * width + x];
                    }
                return pixSum / pixCount;
            }

            let T3 = average(targetT1, centralMask);
            let T4 = average(sourceT1, centralMask);
            let ecv = [];
            for (let i = 0; i < 6; i++) {
                let T1 = average(targetT1, masks[i]);
                let T2 = average(sourceT1, masks[i]);
                ecv[i] = (1 - hematocrit) * (1 / T1 - 1 / T2) / (1 / T3 - 1 / T4);
            }

            let ECVMap = [];
            for (let i = 0; i < height * width; i++) {
                if (targetT1[i] === 0 || sourceT1[i] === 0)
                    ECVMap[i] = 0;
                else {
                    ECVMap[i] = (1 - hematocrit) * (1 / targetT1[i] - 1 / sourceT1[i]) / (1 / T3 - 1 / T4);
                    if (ECVMap[i] < 0)
                        ECVMap[i] = 0;
                    if (ECVMap[i] > 1)
                        ECVMap[i] = 1;
                    ECVMap[i] = Math.round(ECVMap[i] * 500);
                }
            }
            ECVMap[0] = 0;
            ECVMap[1] = 500;

            showECV(ecv, ECVMap);
        }
        document.getElementById("cover-spin").classList.toggle("show");
    };
    xhttp.open("POST", "optf", true);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.send(JSON.stringify({
        parameters: dicoms[(inverse) % 2].parameters,
        targetSegmented: Object.values(dicoms[(1 + inverse) % 2].segmented),
        sourceSegmented: Object.values(dicoms[(inverse) % 2].segmented),
        sourceOriginal: Object.values(dicoms[(inverse) % 2].pixelData)
    }));
}