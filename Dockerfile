FROM node:16-alpine
RUN apk add --no-cache python3
RUN apk add --no-cache make
RUN apk add --no-cache g++
RUN apk add --no-cache gcc
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 8080
CMD ["node","app.js"]