#pragma once

#include <cmath>

#include "common.h"

void getGrad(const Array2D_int& I, Array2D& grad, int N1, int N2, double h1, double h2, float K = 7e-7)
{
	double E0,N0,S0,W0,grI;

	for (int i=1; i<N1-1;i++){			//funkce g0
		for (int j=1; j<N2-1; j++){
			E0= pow((1.0/ h1)*(I[i][j+1] - I[i][j]),2) + pow((1.0/ h2)*(I[i+1][j]+I[i+1][j+1]-I[i-1][j] -I[i-1][j+1]),2) ;
			N0= pow((1.0/ h1)*(I[i][j] - I[i-1][j]),2) + pow((1.0/ h2)*(I[i-1][j+1]+I[i][j+1]-I[i-1][j-1] -I[i][j-1]),2) ;
			W0= pow((1.0/ h1)*(I[i][j] - I[i][j-1]),2) + pow((1.0/ h2)*(I[i+1][j-1]+I[i+1][j]-I[i-1][j] -I[i-1][j-1]),2) ;
			S0= pow((1.0/ h1)*(I[i+1][j] - I[i][j]),2) + pow((1.0/ h2)*(I[i][j+1]+I[i+1][j+1]-I[i][j-1] -I[i+1][j-1]),2) ;

			grI = 0.25*(E0 + N0 + S0 + W0);
			grad[i][j] = 1.0/(1.0+K*grI);
			}
		}
}
//_____________________________________________________________________________________________________________________________________________________________________________

void inicializace(Array2D& u, int stred1, int stred2, int polomer, int N1, int N2, double h1)
{
	for (int k=0; k<N1;k++){
		for (int l=0; l<N2; l++){
			u[k][l]= h1*(sqrt(pow((stred1-k),2)+pow((stred2-l),2)) - polomer);
				}
		}
}
//_____________________________________________________________________________________________________________________________________________________________________________

void segment(Array2D& u, Array2D& v, const Array2D& grad, int n1, int n2, int rozsah, double h1, double h2, int pocetIteraci)
{
	double clen1,clen2,clen3;
	double a1,a2,E,N,W,S,g1,g2,g3,g4,norma,rezid;
	float e = 0.0001;
	float f = 2.0;
	double t = 2.0e-6;


	for (int q = 0; q<pocetIteraci; q++){

		for (int i=n1; i<=n1 + rozsah; i++){
			for (int j=n2; j<= n2 + rozsah; j++){

		// upwinding ve clenech 1 a 2
				a1 =(grad[i][j+1]-grad[i][j-1])/(2*h1);	// x-
				a2 =(grad[i+1][j]-grad[i-1][j])/(2*h2);	// y-
				if(a1>0)	clen1 = a1*((u[i][j+1]-u[i][j])/h1);
				else		clen1 = a1*((u[i][j]-u[i][j-1])/h1);
				if(a2>0)	clen2 = a2*((u[i+1][j]-u[i][j])/h2);
				else		clen2 = a2*((u[i][j]-u[i-1][j])/h2);

		// vypocet clenu 3
				E= pow((1.0/h1)*(u[i][j+1] - u[i][j]),2) + pow((1.0/h2)*(u[i+1][j]+u[i+1][j+1]-u[i-1][j] -u[i-1][j+1]),2) ;
				N= pow((1.0/h1)*(u[i][j] - u[i-1][j]),2) + pow((1.0/h2)*(u[i-1][j+1]+u[i][j+1]-u[i-1][j-1] -u[i][j-1]),2) ;
				W= pow((1.0/h1)*(u[i][j] - u[i][j-1]),2) + pow((1.0/h2)*(u[i+1][j-1]+u[i+1][j]-u[i-1][j] -u[i-1][j-1]),2) ;
				S= pow((1.0/h1)*(u[i+1][j] - u[i][j]),2) + pow((1.0/h2)*(u[i][j+1]+u[i+1][j+1]-u[i][j-1] -u[i+1][j-1]),2) ;
				norma = sqrt(e*e + 0.25*(E+N+S+W));

				g1 =1.0/ sqrt(e*e + E);	/// E = (norma gradientu u na hrane E )^2
				g2 =1.0/ sqrt(e*e + N);
				g3 =1.0/ sqrt(e*e + W);
				g4 =1.0/ sqrt(e*e + S);

				clen3 =	 (1/(h1*h1))*(g1*(u[i][j+1] - u[i][j]) - g3*(u[i][j] - u[i][j-1]))
		  	 		+(1/(h2*h2))*(g4*(u[i+1][j] - u[i][j]) - g2*(u[i][j] - u[i-1][j]));

				v[i][j]=u[i][j] + t*(clen1 + clen2)+ t*grad[i][j]*norma*f*clen3;

			}
		}

		/*for (int j=0; j<N2-1; j++){
		   	v[0][j]=u[1][j];}

		for (int j=1; j<N2-1; j++){
		  	v[N1-1][j]=u[N1-2][j];}

		for (int i=1; i<N1-1;i++){
		   	 v[i][0]=u[i][1];}

		for (int i=1; i<N1-1;i++){
		    	v[i][N2-1]=u[i][N2-2];}*/

		rezid=0.0;
		for (int i=n1; i<=n1 + rozsah; i++){
			for (int j=n2; j<= n2 + rozsah; j++){
				rezid = rezid + (u[i][j]-v[i][j])*(u[i][j]-v[i][j]);
				u[i][j]=v[i][j];
			}
		}

		// vypis z GUI nema smysl
		//if(q%1000 ==0){
		//	cout << rezid << endl;
		//}
    


	}

}
//_____________________________________________________________________________________________________________________________________________________________________________

