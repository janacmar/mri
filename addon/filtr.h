#pragma once

#include <cmath>
#include "common.h"

int max_obr(Array2D& u, int N1, int N2, int* mi, int* mj)
{
    int maxV = u[0][0];

    for (int i=0; i<N1; i++)
    {
        for (int j=0; j<N2; j++)
        {
             if(maxV<u[i][j])
             {
                 maxV = u[i][j];
                 *mi = i;
                 *mj = j;
             }
        }
    }
    return maxV;
}


void ekvalizace(Array2D& u, int N1, int N2)
{
    // EKVALIZACE : priprava

    
    
    int maxVal,mi,mj;

    
    for(int i =0;i<3;i++)
    {
        maxVal = max_obr(u,N1,N2,&mi,&mj);
        u[mi][mj] = 0;
    }
    
    if(maxVal<=300)
    {
    
        // MSVC does not support variable-length arrays
//        int sum[maxVal] = {};
//        double ratio[maxVal];
        Array1D_int sum(maxVal);
        Array1D ratio(maxVal);

        int cumsum = 0;

        //Ekvalizace:
        
        for (int i=0; i<N1; i++)
        {
            for (int j=0; j<N2; j++)
            {
                sum[int(u[i][j])] ++;
            }
        }
        
        for (int i=0; i<= maxVal; i++)
        {
            cumsum = cumsum + sum[i];
            ratio[i] = float(cumsum)/float(N1*N2);
        }
        
        for (int i=0; i<N1; i++)
        {
            for (int j=0; j<N2; j++)
            {
                u[i][j] = ceil(ratio[int(u[i][j])]*maxVal);
            }
        }
        
    }
    else
    {
        
        maxVal = 300;
        
        // MSVC does not support variable-length arrays
//        int sum[maxVal] = {};
//        double ratio[maxVal];
        Array1D_int sum(maxVal);
        Array1D ratio(maxVal);

        int cumsum = 0;
        int overMax = 0;
        
        //Ekvalizace:

        for (int i=0; i<N1; i++)
        {
            for (int j=0; j<N2; j++)
            {
                if(u[i][j]>maxVal)
                {
                    u[i][j] = maxVal;
                }
                sum[int(u[i][j])] ++;
            }
        }
        
        for (int i=0; i<= maxVal; i++)
        {
            cumsum = cumsum + sum[i];
            ratio[i] = float(cumsum)/float(N1*N2);
        }
        
        for (int i=0; i<N1; i++)
        {
            for (int j=0; j<N2; j++)
            {
                if(u[i][j]<=maxVal)
                    u[i][j] = ceil(ratio[int(u[i][j])]*maxVal);
                else
                {
                    u[i][j]=maxVal;
                }
            }
        }
        
        
    }
}




void ekvalizaceVyrez(Array2D_int& u, int N1, int N2, int n1, int n2, int rozsah)
{
    // EKVALIZACE : priprava
    int maxVal,mi,mj;
    
    maxVal = u[0][0];

    for (int i=0; i<N1; i++)
    {
        for (int j=0; j<N2; j++)
        {
             if(maxVal<u[i][j])
             {
                 maxVal = u[i][j];
             }
        }
    }

    // MSVC does not support variable-length arrays
//    int sum[maxVal] = {};
//    double ratio[maxVal];
    Array1D_int sum(maxVal + 1);
    Array1D ratio(maxVal + 1);

    int cumsum = 0;

    //Ekvalizace:
    if(maxVal<=200)
    {
        for (int i=n1; i<=n1+rozsah; i++)
        {
            for (int j=n2; j<n2 + rozsah; j++)
            {
                sum[int(u[i][j])] ++;
            }
        }

        for (int i=0; i<= maxVal; i++)
        {
            cumsum = cumsum + sum[i];
            ratio[i] = float(cumsum)/float((rozsah+1)*(rozsah+1));
        }
        for (int i=n1; i<=n1+rozsah; i++)
        {
            for (int j=n2; j<n2 + rozsah; j++)
            {
                u[i][j] = ceil(ratio[int(u[i][j])]*maxVal);
            }
        }
        
    }
    else
    {
        
        maxVal = 200;
        
        // MSVC does not support variable-length arrays
//        int sum[maxVal] = {};
//        double ratio[maxVal];
        Array1D_int sum(maxVal + 1);
        Array1D ratio(maxVal + 1);

        int cumsum = 0;
                
        //Ekvalizace:
        for (int i=n1; i<=n1+rozsah; i++)
        {
            for (int j=n2; j<n2 + rozsah; j++)
            {
                if(u[i][j]>maxVal)
                {
                    u[i][j] = 0;
                }
                sum[int(u[i][j])] ++;
            }
        }

        for (int i=0; i<= maxVal; i++)
        {
            cumsum = cumsum + sum[i];
            ratio[i] = float(cumsum)/float((rozsah+1)*(rozsah+1));
        }
        for (int i=n1; i<=n1+rozsah; i++)
        {
            for (int j=n2; j<n2 + rozsah; j++)
            {
                u[i][j] = ceil(ratio[int(u[i][j])]*maxVal);
            }
        }
    }
}




// u: obrazek
// N1: pocet radku
// N2: pocet sloupcu
void filtr(Array2D& u, int N1, int N2)
{
	double t=1.5e-8;
	float e=0.0001;

    // MSVC does not support variable-length arrays
//	float v[N1][N2];		// N1 radku na N2 sloupcu
	Array2D v(N1, Array1D(N2));
	float h1=1.0/fmax(N1, N2);
	float h2=h1;
    
        
    // Difuze:
    for (int n=1; n<=60; n++)
    {

        #pragma omp parallel for collapse(2) schedule(static)
        for (int i=1; i<N1-1;i++){
                for (int j=1; j<N2-1; j++){
                    double E=pow(((u[i+1][j]-u[i][j])/h1),2)+pow(((u[i][j+1]+u[i+1][j+1]-u[i][j-1]-u[i+1][j-1])/(4*h2)),2);
                    double N=pow(((u[i+1][j-1]+u[i+1][j]-u[i-1][j-1]-u[i-1][j])/(4*h1)),2)+pow(((u[i][j]-u[i][j-1])/h2),2);
                    double W=pow(((u[i][j]-u[i-1][j])/h1),2)+pow(((u[i-1][j+1]+u[i][j+1]-u[i-1][j-1]-u[i][j-1])/(4*h2)),2);
                    double S=pow(((u[i+1][j]+u[i+1][j+1]-u[i-1][j]-u[i-1][j+1])/(4*h1)),2)+pow(((u[i][j+1]-u[i][j])/h2),2);

                    double g1=sqrt(1/(e*e+E));
                    double g2=sqrt(1/(e*e+N));
                    double g3=sqrt(1/(e*e+W));
                    double g4=sqrt(1/(e*e+S));

                    double norma=(sqrt(E)+sqrt(N)+sqrt(W)+sqrt(S))/4;


                    v[i][j]=u[i][j]+ norma*t*((1/(h1*h1))*(g1*(u[i+1][j]-u[i][j])-g3*(u[i][j]-u[i-1][j]))
                            +(1/(h2*h2))*(-g2*(u[i][j]-u[i][j-1])+g4*(u[i][j+1]-u[i][j])));
                        }
                    }

        // okraje
            #pragma omp parallel for schedule(static)
            for (int j=1; j<N2-1; j++) {
                v[0][j]=u[1][j];
                v[N1-1][j]=u[N1-2][j];
            }
            #pragma omp parallel for schedule(static)
            for (int i=0; i<N1; i++) {
                v[i][0]=u[i][1];
                v[i][N2-1]=u[i][N2-2];
            }
            // (rohy neni treba resit samostatne, kdyz se predchozi cyklus projede od 0 do N1-1)

        // zamena matic
            #pragma omp parallel for collapse(2) schedule(static)
            for (int k=0; k<N1;k++){
                for (int l=0; l<N2; l++){
                    u[k][l]=v[k][l];
                        }
                    }

    }
	
 
}


// u: obrazek
// N1: pocet radku
// N2: pocet sloupcu
void filtrZaver(Array2D_int& u, int N1, int N2)
{
	double t=1.0e-10;
	float e=0.0001;

    // MSVC does not support variable-length arrays
//	float v[N1][N2];		// N1 radku na N2 sloupcu
	Array2D v(N1, Array1D(N2));
	float h1=1.0/fmax(N1, N2);
	float h2=h1;
    
        
    // Difuze:
    for (int n=1; n<=30; n++)
    {
        #pragma omp parallel for collapse(2) schedule(static)
        for (int i=1; i<N1-1;i++){
                for (int j=1; j<N2-1; j++){
                    double E=pow(((u[i+1][j]-u[i][j])/h1),2)+pow(((u[i][j+1]+u[i+1][j+1]-u[i][j-1]-u[i+1][j-1])/(4*h2)),2);
                    double N=pow(((u[i+1][j-1]+u[i+1][j]-u[i-1][j-1]-u[i-1][j])/(4*h1)),2)+pow(((u[i][j]-u[i][j-1])/h2),2);
                    double W=pow(((u[i][j]-u[i-1][j])/h1),2)+pow(((u[i-1][j+1]+u[i][j+1]-u[i-1][j-1]-u[i][j-1])/(4*h2)),2);
                    double S=pow(((u[i+1][j]+u[i+1][j+1]-u[i-1][j]-u[i-1][j+1])/(4*h1)),2)+pow(((u[i][j+1]-u[i][j])/h2),2);

                    double g1=sqrt(1/(e*e+E));
                    double g2=sqrt(1/(e*e+N));
                    double g3=sqrt(1/(e*e+W));
                    double g4=sqrt(1/(e*e+S));

                    double norma=(sqrt(E)+sqrt(N)+sqrt(W)+sqrt(S))/4;


                    v[i][j]=u[i][j]+ norma*t*((1/(h1*h1))*(g1*(u[i+1][j]-u[i][j])-g3*(u[i][j]-u[i-1][j]))
                            +(1/(h2*h2))*(-g2*(u[i][j]-u[i][j-1])+g4*(u[i][j+1]-u[i][j])));
                        }
                    }

        // okraje
            #pragma omp parallel for schedule(static)
            for (int j=1; j<N2-1; j++) {
                v[0][j]=u[1][j];
                v[N1-1][j]=u[N1-2][j];
            }
            #pragma omp parallel for schedule(static)
            for (int i=0; i<N1; i++) {
                v[i][0]=u[i][1];
                v[i][N2-1]=u[i][N2-2];
            }
            // (rohy neni treba resit samostatne, kdyz se predchozi cyklus projede od 0 do N1-1)

        // zamena matic
            #pragma omp parallel for collapse(2) schedule(static)
            for (int k=0; k<N1;k++){
                for (int l=0; l<N2; l++){
                    u[k][l]=v[k][l];
                        }
                    }

    }
	
 
}


