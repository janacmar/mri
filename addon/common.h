#pragma once

#include <vector>

using Array1D = std::vector<double>;
using Array2D = std::vector<Array1D>;

using Array1D_int = std::vector<int>;
using Array2D_int = std::vector<Array1D_int>;
