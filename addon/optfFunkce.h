#pragma once

#include <cmath>

#include "common.h"

void normy(const Array2D& I, const Array2D& J, double *L1, double *L2, int n1, int n2, int rozsah)
{
	*L1 = 0.0;
	*L2 = 0.0;

	for (int i= n1; i<= n1 + rozsah;i++){
		for (int j=n2; j<= n2 + rozsah; j++){
			*L2 = *L2 + (J[i][j] - I[i][j])*(J[i][j] - I[i][j]);
		}
	}
	*L2= sqrt(*L2);

	////////////////// L1 norma
	for (int i= n1; i<= n1 + rozsah;i++){
		for (int j=n2; j<= n2 + rozsah; j++){
			if((J[i][j] - I[i][j])>=0)
				*L1 = *L1 + (J[i][j] - I[i][j]);
			else
				*L1 = *L1 + (-J[i][j] + I[i][j]);
		}
	}
}

void optf(Array2D& u, Array2D& v, const Array2D& I, const Array2D& J, int n1, int n2, int N1, int N2, int rozsah, double h1, double h2,
		int pocet_iteraci, double e, double t, double alfa, double beta, double gama)
{
	double h=1.0;

	Array2D u_n(N1, Array1D(N2));
	Array2D v_n(N1, Array1D(N2));

	int iter=0;
	while(iter < pocet_iteraci){
		iter = iter + 1;

		for (int i=n1 +1; i<n1 + rozsah ; i++){
			for (int j=n2 +1; j<n2 + rozsah ; j++){

				double Eu=pow(((u[i+1][j]-u[i][j])/h1),2)+pow(((u[i][j+1]+u[i+1][j+1]-u[i][j-1]-u[i+1][j-1])/(4*h2)),2);
				double Nu=pow(((u[i+1][j-1]+u[i+1][j]-u[i-1][j-1]-u[i-1][j])/(4*h1)),2)+pow(((u[i][j]-u[i][j-1])/h2),2);
				double Wu=pow(((u[i][j]-u[i-1][j])/h1),2)+pow(((u[i-1][j+1]+u[i][j+1]-u[i-1][j-1]-u[i][j-1])/(4*h2)),2);
				double Su=pow(((u[i+1][j]+u[i+1][j+1]-u[i-1][j]-u[i-1][j+1])/(4*h1)),2)+pow(((u[i][j+1]-u[i][j])/h2),2);

				double Ev=pow(((v[i+1][j]-v[i][j])/h1),2)+pow(((v[i][j+1]+v[i+1][j+1]-v[i][j-1]-v[i+1][j-1])/(4*h2)),2);
				double Nv=pow(((v[i+1][j-1]+v[i+1][j]-v[i-1][j-1]-v[i-1][j])/(4*h1)),2)+pow(((v[i][j]-v[i][j-1])/h2),2);
				double Wv=pow(((v[i][j]-v[i-1][j])/h1),2)+pow(((v[i-1][j+1]+v[i][j+1]-v[i-1][j-1]-v[i][j-1])/(4*h2)),2);
				double Sv=pow(((v[i+1][j]+v[i+1][j+1]-v[i-1][j]-v[i-1][j+1])/(4*h1)),2)+pow(((v[i][j+1]-v[i][j])/h2),2);


				double g1=1.0/sqrt(e*e+Eu +Ev);
				double g2=1.0/sqrt(e*e+Nu +Nv);
				double g3=1.0/sqrt(e*e+Wu +Wv);
				double g4=1.0/sqrt(e*e+Su +Sv);
//__________________________________________________________________________________________________________________________________________________________

				double vyhlazeni=((1/(h1*h1))*(g1*(u[i+1][j]-u[i][j])-g3*(u[i][j]-u[i-1][j]))
					+(1/(h2*h2))*(-g2*(u[i][j]-u[i][j-1])+g4*(u[i][j+1]-u[i][j])));	// vyhlazeni

				double brightness = ( ((I[i][j+1]-I[i][j])/h1)*u[i][j]+((I[i+1][j]-I[i][j])/h2)*v[i][j]+((J[i][j]-I[i][j])/h))*
				((I[i][j]-I[i][j-1])/h1);	 // brightness cocnstancy

				u_n[i][j] = u[i][j]  + t*( beta*vyhlazeni - gama*u[i][j] - alfa*brightness );

//__________________________________________________________________________________________________________________________________________________________

				vyhlazeni=0.0;
				brightness=0.0;
				vyhlazeni=((1/(h1*h1))*(g1*(v[i+1][j]-v[i][j])-g3*(v[i][j]-v[i-1][j]))
					+(1/(h2*h2))*(-g2*(v[i][j]-v[i][j-1])+g4*(v[i][j+1]-v[i][j])));

				brightness=( ((I[i][j+1]-I[i][j])/h1)*u[i][j]+((I[i+1][j]-I[i][j])/h2)*v[i][j]+((J[i][j]-I[i][j])/h))*
					((I[i][j]-I[i-1][j])/h1);

				v_n[i][j] = v[i][j]  + t* ( beta*vyhlazeni - gama*v[i][j] - alfa*brightness );
			}
		}


		for (int j=n2; j< n2 + rozsah; j++){	u[n1][j]=u_n[n1+1][j];}
		for (int j=n2; j< n2 + rozsah; j++){	u[n1 + rozsah][j]=u_n[n1 + rozsah-1][j];}

		for (int i=n1; i< n1 + rozsah;i++){	u[i][n2]=u_n[i][n2+1];}
		for (int i=n1; i< n1 + rozsah;i++){	u[i][n2 + rozsah]=u_n[i][n2 + rozsah-1];}

		u[n1][n2]=u_n[n1][n2+1];
		u[n1][n2 + rozsah]=u_n[n1][n2 + rozsah-1];

		u[n1 + rozsah][n2]=u_n[n1 + rozsah -1][n2+1];
		u[n1 + rozsah][n2 + rozsah]=u_n[n1 + rozsah-1][n2 + rozsah-1];

		for (int j=n2; j< n2 + rozsah; j++){	v[n1][j]=v_n[n1+1][j];}
		for (int j=n2; j< n2 + rozsah; j++){	v[n1 + rozsah][j]=v_n[n1 + rozsah-1][j];}

		for (int i=n1; i< n1 + rozsah;i++){	v[i][n2]=v_n[i][n2+1];}
		for (int i=n1; i< n1 + rozsah;i++){	v[i][n2 + rozsah]=v_n[i][n2 + rozsah-1];}

		v[n1][n2]=v_n[n1][n2+1];
		v[n1][n2 + rozsah]=v_n[n1][n2 + rozsah-1];

		v[n1 + rozsah][n2]=v_n[n1 + rozsah -1][n2+1];
		v[n1 + rozsah][n2 + rozsah]=v_n[n1 + rozsah-1][n2 + rozsah-1];


		double rezid1 = 0.0;
		double rezid2 = 0.0;
		for (int i=n1;i<=n1+rozsah; i++){
			for (int j=n2; j<n2 + rozsah; j++){
				rezid1 = rezid1 + (u[i][j]-u_n[i][j])*(u[i][j]-u_n[i][j]);
				rezid2 = rezid2 + (v[i][j]-v_n[i][j])*(v[i][j]-v_n[i][j]);
				u[i][j] = u_n[i][j];
				v[i][j] = v_n[i][j];
			}
		}
		double rezid = sqrt(rezid1 + rezid2);

		// vypis z GUI nema smysl
		//if(iter%5000==0)
		//	cout << rezid << endl;
	}
}
