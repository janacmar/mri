#pragma once

#include <cmath>

#include "common.h"

void distance( Array2D& u0, int n1, int n2, int N1, int N2, int rozsah, double h1, double h2, int pocetIteraci)
{
	float e=0.0001;
	float t= 5.0e-7;
	float rezid=100;

	// N1 radku na N2 sloupcu
	Array2D u(N1, Array1D(N2));
	Array2D v(N1, Array1D(N2));

	for (int i=n1; i<=n1+rozsah; i++){
		for (int j=n2; j<=n2+rozsah; j++){
			u[i][j] = u0[i][j];
		}
	}


	int n=0;
	while(n<pocetIteraci){
		n=n+1;

		for (int i=n1+1; i<n1 + rozsah; i++)
		for (int j=n2+1; j<n2 + rozsah; j++)
		{
			double clen1 = 0.0;
			double clen2 = 0.0;
			double clen3 = 0.0;
			double clen4 = 0.0;

			double S = u0[i][j]/(sqrt(e*e + u0[i][j]*u0[i][j]));

			if(S>0){
				clen1 = ((u[i][j] - u[i][j-1])/h1);
				if (clen1 <0){ clen1 = 0.0; };
				clen2 = ((u[i][j+1] - u[i][j])/h1);
				if (clen2 >0){ clen2 = 0.0; };
				if(clen1 < -clen2){clen1 = clen2;}

				clen3 = ((u[i][j] - u[i-1][j])/h2);
				if (clen3 <0){ clen3 = 0.0; };
				clen4 = ((u[i+1][j] - u[i][j])/h2);
				if (clen4 >0){ clen4 = 0.0; };
				if(clen3 < -clen4){clen3 = clen4;}
			}
			else{
				clen1 = ((u[i][j+1] - u[i][j])/h1);
				if (clen1 <0){ clen1 = 0.0; };
				clen2 = ((u[i][j] - u[i][j-1])/h1);
				if (clen2 >0){ clen2 = 0.0; };
				if(clen1 < -clen2){clen1 = clen2;}

				clen3 = ((u[i+1][j] - u[i][j])/h2);
				if (clen3 <0){ clen3 = 0.0; };
				clen4 = ((u[i][j] - u[i-1][j])/h2);
				if (clen4 >0){ clen4 = 0.0; };
				if(clen3 < -clen4){clen3 = clen4;}
			}

			v[i][j] = u[i][j] + t*S*( 1.0 - sqrt(clen1*clen1 + clen3*clen3));
		}


		for (int j=n2+1; j<=n2 + rozsah; j++){	v[n1][j]=u[n1+1][j];}
		for (int j=n2+1; j<=n2 + rozsah; j++){	v[n1 + rozsah][j]=u[n1 + rozsah-1][j];}

		for (int i=n1+1; i<=n1 + rozsah; i++){	v[i][n2]=u[i][n2+1];}
		for (int i=n1+1; i<=n1 + rozsah; i++){	v[i][n2 + rozsah]=u[i][n2 + rozsah-1];}

		v[n1][n2]=u[n1][n2+1];
		v[n1][n2 + rozsah]=u[n1][n2 + rozsah-1];

		v[n1 + rozsah][n2]=u[n1 + rozsah -1][n2+1];
		v[n1 + rozsah][n2 + rozsah]=u[n1 + rozsah-1][n2 + rozsah-1];


		rezid=0.0;
		for (int i=n1; i<=n1 + rozsah; i++){
			for (int j=n2; j<=n2 + rozsah; j++){
				rezid = rezid +(v[i][j] - u[i][j])*(v[i][j] - u[i][j]);
				u[i][j] = v[i][j];
			}
		}
		rezid = sqrt(rezid);

		// vypis z GUI nema smysl
		//if(n%10000==0){	cout << rezid << endl;}
	}

	for (int i=n1; i<=n1+rozsah; i++){
		for (int j=n2; j<=n2+rozsah; j++){
			u0[i][j] = v[i][j];
		}
	}

}
