#include <napi.h>
#include <stdio.h>
#include <iostream>
#include "filtr.h"
#include "segmentFunkce.h"
#include "distFunkce.h"
#include "optfFunkce.h"

Napi::ArrayBuffer level_set(const Napi::CallbackInfo & info) {
    Napi::Env env = info.Env();
    float polomer_vnejsi = info[0].As<Napi::Number>();
    float polomer_vnitrni = info[1].As<Napi::Number>();
    int stred1 = info[2].As<Napi::Number>();
    int stred2 = info[3].As<Napi::Number>();
    float K1 = info[4].As<Napi::Number>();
    float K2 = info[5].As<Napi::Number>();
    const int N1 = info[6].As<Napi::Number>();
    const int N2 = info[7].As<Napi::Number>();

    Napi::ArrayBuffer buf = info[8].As<Napi::ArrayBuffer>();
    double * data = reinterpret_cast<double*>(buf.Data());

    Array2D u(N1, Array1D(N2));
    for (int i = 0; i < N1; i++)
        for (int j = 0; j < N2; j++)
            u[i][j] = data[i * N2 + j];

    filtr(u, N1, N2);

    const int n1 = stred1 - polomer_vnejsi - 15; // svisle
    const int n2 = stred2 - polomer_vnejsi - 15; // vodorovne
    const int rozsah = 2*(polomer_vnejsi + 15);
    const double h1 = 1.0 / std::max(N1, N2);
    const double h2 = h1;

    Array2D_int A(N1, Array1D_int(N2));
    for (int i = 0; i < N1; i++)
        for (int j = 0; j < N2; j++)
            A[i][j] = std::round(u[i][j]);

    Array2D grad(N1, Array1D(N2));
    Array2D u_vnejsi(N1, Array1D(N2));
    Array2D u_vnitrni(N1, Array1D(N2));
    Array2D v(N1, Array1D(N2));

//filtrovani druha faze ________________________

    ekvalizaceVyrez(A,N1,N2,n1,n2,rozsah);
    filtrZaver(A,N1,N2);
//_____________________________________________-

    // vnejsi
//    float K = 1.5e-6;
    getGrad(A, grad, N1, N2, h1, h2, K1);

    inicializace(u_vnejsi, stred1, stred2, polomer_vnejsi, N1, N2, h1);

    segment(u_vnejsi, v, grad, n1, n2, rozsah, h1, h2, 6000);
    distance(u_vnejsi, n1, n2, N1, N2, rozsah, h1, h2, 3.0e4);

    // modifikace - vnejsi -> vnitrni
    for (int i=n1; i<=n1 + rozsah; i++)
    for (int j=n2; j<=n2 + rozsah; j++)
        u_vnitrni[i][j] = u_vnejsi[i][j] + (polomer_vnejsi - polomer_vnitrni) * h1;


    // vnitrni
//    K = 4.0e-6;
    getGrad(A, grad, N1, N2, h1, h2, K2);
    segment(u_vnitrni, v, grad, n1, n2, rozsah, h1, h2, 1200);
    // now u_vnitrni == v

    // mezikruzi
    for (int i=n1; i<=n1 + rozsah; i++)
    for (int j=n2; j<=n2 + rozsah; j++)
        if(u_vnitrni[i][j]<0)
            v[i][j] = -u_vnitrni[i][j];     // vnitrek
        else
            v[i][j] = u_vnejsi[i][j];       // vnejsek

    distance(v, n1, n2, N1, N2, rozsah, h1, h2, 3.0e4);

    // normalizace pro vystup
    #pragma omp parallel for collapse(2) schedule(static)
    for (int i=0; i<N1; i++)
    for (int j=0; j<N2; j++) {
        if(v[i][j]> 10*h1)
            v[i][j] = 10*h1;
        if(v[i][j]< -10*h1)
            v[i][j] = -10*h1;
    }

    for (int i = 0; i < N1; i++)
        for (int j = 0; j < N2; j++)
            data[i * N2 + j] = v[i][j];

    return Napi::ArrayBuffer::New(env, buf.Data(), buf.ByteLength());
}

Napi::ArrayBuffer OPTF(const Napi::CallbackInfo & info) {
    Napi::Env env = info.Env();
    float polomer = info[0].As<Napi::Number>();
    int stred1 = info[1].As<Napi::Number>();
    int stred2 = info[2].As<Napi::Number>();
    const int N1 = info[3].As<Napi::Number>();
    const int N2 = info[4].As<Napi::Number>();

    Napi::ArrayBuffer buf = info[5].As<Napi::ArrayBuffer>();
    double * data = reinterpret_cast<double*>(buf.Data());

    Array2D J(N1, Array1D(N2));
    for (int i = 0; i < N1; i++)
        for (int j = 0; j < N2; j++)
            J[i][j] = data[i * N2 + j];

    Array2D I(N1, Array1D(N2));
    for (int i = 0; i < N1; i++)
        for (int j = 0; j < N2; j++)
            I[i][j] = data[i * N2 + j + N1 * N2];

    Array2D_int T(N1, Array1D_int(N2));
    for (int i = 0; i < N1; i++)
        for (int j = 0; j < N2; j++)
            T[i][j] = std::round(data[i * N2 + j + N1 * N2 * 2]);

    const int n1 = stred1 - polomer - 15; // svisle
    const int n2 = stred2 - polomer - 15; // vodorovne
    const int rozsah = 2*(polomer + 15);
    const double h1 = 1.0 / std::max(N1, N2);
    const double h2 = h1;

    Array2D u(N1, Array1D(N2));
    Array2D v(N1, Array1D(N2));

    // pomocna pole
    Array2D_int p(N1, Array1D_int(N2));

    for (int i = 0; i < N1; i++)
    for (int j = 0; j < N2; j++) {
        u[i][j] = 0.0;
        v[i][j] = 0.0;
        J[i][j] = std::max(N1, N2) * J[i][j];
        I[i][j] = std::max(N1, N2) * I[i][j];
    }

    for (int i = n1 + 1; i < n1 + rozsah; i++)
    for (int j = n2 + 1; j < n2 + rozsah; j++){
        u[i][j] =  0.1 * h1;
        v[i][j] = -0.1 * h1;
    }

    const float gama = 4.5;   // -> minimalizace
    const float alfa = 0.75;  // -> brightness const.
    const float beta = 4.5;   // -> vyhlazení

    const float e = 1.0e-3;
    const float t = 7.0e-8;

    const int pocet_iteraci = 6.0e3;

    optf(u, v, I, J, n1, n2, N1, N2, rozsah, h1, h2, pocet_iteraci, e, t, alfa, beta, gama);

    #pragma omp parallel for collapse(2) schedule(static)
    for (int i = 0; i < N1; i++)
    for (int j = 0; j < N2; j++) {
        const int ra = i - std::round(v[i][j] / h2);
        const int sl = j - std::round(u[i][j] / h1);
        if (ra >= 0 && ra < N1 && sl >= 0 && sl < N2)
            p[i][j] = T[ra][sl];
        else
            p[i][j] = 0;
    }

    for (int i = 0; i < N1; i++)
        for (int j = 0; j < N2; j++)
            data[i * N2 + j] = p[i][j];
    for (int i = 0; i < N1; i++)
        for (int j = 0; j < N2; j++)
            data[i * N2 + j + N1 * N2] = u[i][j];
    for (int i = 0; i < N1; i++)
        for (int j = 0; j < N2; j++)
            data[i * N2 + j + N1 * N2 * 2] = v[i][j];

    return Napi::ArrayBuffer::New(env, buf.Data(), buf.ByteLength());
}

Napi::Object InitAll(Napi::Env env, Napi::Object exports) {
  exports.Set("levelSet", Napi::Function::New(env, level_set));
  exports.Set("optf", Napi::Function::New(env, OPTF));
  return exports;
}

NODE_API_MODULE(mriAddon, InitAll)