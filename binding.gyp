{
    "targets": [{
        "target_name": "mriAddon",
        'msvs_settings': {
            'VCCLCompilerTool': {
                'AdditionalOptions': ['/MT', '/openmp']
            }
        },
        "cflags!": ["-fno-exceptions"],
        "cflags_cc!": ["-fno-exceptions"],
        "sources": [
            "addon/mri.cpp",
            "addon/common.h",
            "addon/filtr.h",
            "addon/segmentFunkce.h",
            "addon/distFunkce.h",
            "addon/optfFunkce.h"
        ],
        'include_dirs': [
            "<!@(node -p \"require('node-addon-api').include\")"
        ],
        'libraries': [],
        'dependencies': [
            "<!(node -p \"require('node-addon-api').gyp\")"
        ],
        'defines': ['NAPI_DISABLE_CPP_EXCEPTIONS']
    }]
}